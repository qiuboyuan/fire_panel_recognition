#coding:utf8
from flask import Flask
app=Flask(__name__)

app.debug=True

from 控制器 import pages

app.register_blueprint(pages)

@app.errorhandler(404)
def page_not_found(error):
    return "<h1>404-未找到该页面。</h1>"
#所有的flask相关配置要在run之前完成，run之后的不予执行
if __name__=="__main__":
    app.run(host='0.0.0.0',port=5000)

