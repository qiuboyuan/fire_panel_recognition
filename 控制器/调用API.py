# coding:utf-8
import urllib,base64,json
from urllib import request as rq
from urllib import parse

def postimg(img,accuracy):
    access_token = '24.916949573a14bb57ba9270d1fa1e613b.2592000.1532330676.282335-11434893'
    url = accuracy + access_token
    # 参数image：图像base64编码
    params = {"image": img}#默认识别语言是中英混合
    params = parse.urlencode(params).encode('utf8')#默认的格式也是utf-8
    request = rq.Request(url, params)
    request.add_header('Content-Type', 'application/x-www-form-urlencoded')
    response = rq.urlopen(request)
    content = response.read()#此时接收到的数据流是bytes格式的字节流而不是字符串，需要用decode函数解码
    if (content):
        return analyzeResponse(content) #对json包解析
    else:
        return False

#自写解析json数据包的函数
def analyzeResponse(content):
    str_content=content.decode() #对字节流解码成字符串
    print("收到响应结果："+str_content)
    dict_content=json.loads(str_content) #将字符串转化为字典
    wordinfo= dict_content['words_result']#一个列表list
    # location=wordinfo['location'] #location还是字典
    # words=wordinfo['words'] #words已经是字符串了
    # print("位置信息："+json.dumps(location))
    # print("文字信息："+words)
    return wordinfo
