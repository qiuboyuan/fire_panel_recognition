from flask import render_template,request
from . import pages
from . import 调用API as dy
import json,base64
@pages.route("/")
def main():
    return render_template("main.html")

@pages.route("/analyseimg",methods=["POST"])
def analyseimg():
    print("后台接收到了分析图象的请求")
    # 二进制方式打开图文件
    # f = open(r'static/img1.jpg', 'rb')
    # img = base64.b64encode(f.read())
    
    acimg=json.loads(request.form.get('acimg')) 
    str_img=acimg['img'].replace('data:image/jpeg;base64,','').replace(' ','+') 
    img=bytes(str_img, encoding='utf-8')
    ac=acimg['ac']
    print(img)
    # print(ac) #判断值
    # print(type(ac))   #判断收到的值类型
    if(ac==1):
        print("执行一般识别")
        accuracy='https://aip.baidubce.com/rest/2.0/ocr/v1/general?access_token='
        wordinfo=dy.postimg(img,accuracy)
        # print(len(wordinfo))
    elif(ac==2):
        print("执行精确识别")
        accuracy='https://aip.baidubce.com/rest/2.0/ocr/v1/accurate?access_token='
        wordinfo=dy.postimg(img,accuracy)
    else:
        print("不执行识别")
        wordinfo=None
    if wordinfo!=None:
        return json.dumps(wordinfo)#把字典转换成字json字节流
    else:
        return "没有信息"